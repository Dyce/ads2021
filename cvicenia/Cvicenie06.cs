﻿using System;
using System.IO;
using System.Linq;

namespace cvicenia
{
    public class Cvicenie06
    {
        public Cvicenie06()
        {
            ReadData();
            int result = GetResult();
            Console.WriteLine(result);
        }

        private int GetResult()
        {
            int[,] matrix = new int[tokens.Length, tokens.Length]; 
            for(int l = 0; l < tokens.Length; l++)
            {
                for(int j = l; j < tokens.Length; j++)
                {
                    int i = j - l;
                    if(j == i)
                        matrix[i, j] = tokens[i];
                    else if(j == i + 1)
                        matrix[i, j] = Math.Max(tokens[i], tokens[j]);
                    else
                    {
                        int opt1 = 0, opt2 = 0;
                        if(tokens[j] >= tokens[i + 1] && i+1 <= j-1)
                            opt1 = tokens[i] + matrix[i + 1, j - 1];
                        else if (i+2 <= j)
                            opt1 = tokens[i] + matrix[i + 2, j];

                        if(tokens[j - 1] >= tokens[i] && i <= j - 2)
                            opt2 = tokens[j] + matrix[i, j - 2];
                        else if (i + 1 <= j - 1)
                            opt2 = tokens[j] + matrix[i + 1, j - 1];

                        matrix[i, j] = Math.Max(opt1, opt2);
                    }
                }
            }
            var result = from int item in matrix
                         where item == 2780 
                         select item;
            return matrix[0, matrix.GetLength(1) - 1];
        }

        int[] tokens;
        private void ReadData()
        {
            using(StreamReader sr = new StreamReader(@"C:\Users\Michal\source\repos\ADS\cvicenia\cvicenia\cvicenie06data\cvicenie6data.txt"))
            {
                string stringData = sr.ReadLine();
                tokens = stringData.ToCharArray().Select(x => int.Parse(x.ToString())).ToArray();
            }
        }
    }
}
