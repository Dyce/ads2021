﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace cvicenia
{
    public class Cvicenie04
    {
        public Cvicenie04()
        {
            ReadData();
            int result = GetResult();
            Console.WriteLine($"Max value: {result}");
        }

        class Item
        {
            public int Value { get; set; }
            public int Weight { get; set; }
        }

        List<List<Item>> data = new List<List<Item>>();
        int[,] maxVal;
        private void ReadData()
        {
            using(StreamReader sr = new StreamReader(@"C:\Users\Michal\source\repos\ADS\cvicenia\cvicenia\cvicenie04data\cvicenie04data.txt"))
            {
                while(!sr.EndOfStream)
                {
                    string[] rowData = sr.ReadLine().Split(',');
                    data.Add(new List<Item>());
                    data.Last().Add(new Item
                    {
                        Value = int.Parse(rowData[0]),
                        Weight = int.Parse(rowData[1])
                    });
                    data.Last().Add(new Item
                    {
                        Value = int.Parse(rowData[2]),
                        Weight = int.Parse(rowData[3])
                    });
                }
            }
        }

        private int GetResult()
        {
            int capacity = 2000;
            maxVal = new int[data.Count + 1 , capacity + 1];

            for(int i = 0; i < data.Count + 1; i++)
            {
                for(int w = 0; w < capacity + 1; w++)
                {
                    if(i * w == 0)
                        continue;

                    int tmpMax = maxVal[i - 1, w];

                    if(data[i - 1][0].Weight <= w)
                        tmpMax = Math.Max(tmpMax, data[i - 1][0].Value + maxVal[i - 1, w - data[i - 1][0].Weight]);
                    if(data[i - 1][1].Weight <= w)
                        tmpMax = Math.Max(tmpMax, data[i - 1][1].Value + maxVal[i - 1, w - data[i - 1][1].Weight]);
                    maxVal[i, w] = tmpMax;
                }
            }
            return maxVal[data.Count, capacity];
        }
    }
}
