﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;

namespace cvicenia
{
    public class Cvicenie01
    {
        StreamReader sr;
        int[,] matrix;
        public Cvicenie01()
        {
            ReadData();
            int bestsum = int.MaxValue;
            int i = 1;
            while(true)
            {
                i-=-1;
                int sum = GetSolution();
                if(Math.Abs(sum) < bestsum)
                {
                    Console.WriteLine($"Iteration {i} sum: {sum}");
                    bestsum = Math.Abs(sum);
                }
                if(sum == 0)
                    break;
                //Console.WriteLine("Shuffling rows...");
                shuffleRows(matrix);
            }

        }

        private int GetSolution()
        {
            int previousSum = int.MaxValue;
            int[] elements = new int[matrix.GetLength(0)];

            elements[0] = matrix[0, 0];
            int sum = elements[0];

            for(int i = 1; i < matrix.GetLength(0); i++)
            {
                int bestNumber = matrix[i, 0];
                int bestSum = sum + bestNumber;

                for(int k = 1; k < matrix.GetLength(1); k++)
                {
                    if(Math.Abs(matrix[i, k] + sum) < Math.Abs(bestSum))
                    {
                        bestSum = matrix[i, k] + sum;
                        bestNumber = matrix[i, k];
                    }
                }
                elements[i] = bestNumber;
                sum = bestSum;
            }

            //Console.WriteLine($"First iteration sum: {sum}");
            int coutner = 0;
            while(previousSum != sum)
            {
                coutner++;
                previousSum = sum;
                for(int i = 0; i < matrix.GetLength(0); i++)
                {
                    int tempSum = sum;
                    for(int k = 0; k < matrix.GetLength(1); k++)
                    {
                        if(Math.Abs(tempSum - elements[i] + matrix[i,k]) < Math.Abs(sum))
                        {
                            tempSum = tempSum - elements[i] + matrix[i, k];
                            elements[i] = matrix[i, k];
                        }
                    }
                    sum = tempSum;
                }
                //Console.WriteLine($"Iteration {coutner} sum: {sum}");

            }
            return sum;
        }

        private void shuffleRows(int[,] array)
        {
            Random r = new Random();
            for(int i = 0; i < array.GetLength(0); i++)
            {
                int randomRow = r.Next(i, array.GetLength(0));
                for(int k = 0; k < array.GetLength(1); k++)
                {
                    swap(matrix, i, k, randomRow, k);
                }
            }
        }

        private void swap(int[,] array, int r1, int c1, int r2, int c2)
        {
            int temp = array[r1, c1];
            array[r1, c1] = array[r2, c2];
            array[r2, c2] = temp;
        }

        private void ReadData()
        {
            using (sr = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\..\cvicenia\cvicenie1data.txt")))
            //using(sr = new StreamReader(@"C:\Users\Michal\source\repos\ADS\cvicenia\cvicenia\output-onlinemathtools.txt"))
            {
                int rows = Int32.Parse(sr.ReadLine());
                int cols = Int32.Parse(sr.ReadLine());
                matrix = new int[rows, cols];
                for (int i = 0; i < rows; i++)
                {
                    string[] row = sr.ReadLine().Split(' ');
                    for (int k = 0; k < cols; k++)
                    {
                        matrix[i, k] = int.Parse(row[k]);
                    }
                }
            }
        }
    }
}
