﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace cvicenia
{
    class Cvicenie02
    {
        int[] cities = new int[1000];
        long[] bestPaths = new long[1000];
        public Cvicenie02()
        {
            ReadData();
            long result = GetResult();
            Console.WriteLine($"\nMinimum penalty is: {result} ({Math.Round(Math.Sqrt(result), 2)})");
        }

        private long GetResult()
        {
            for(int i = 0; i < 1000; i++)
            {
                bestPaths[i] = CalculatePenalty(cities[i]);
                for(int j = 0; j < i; j++)
                        bestPaths[i] = Math.Min(bestPaths[i],bestPaths[j] + CalculatePenalty(cities[i] - cities[j]));
            }
            return bestPaths[999];
        }

        private long CalculatePenalty(int n) => (long)Math.Pow(400 - n, 2);

        private void ReadData()
        {
            using(StreamReader sr = new StreamReader(@"C:\Users\Michal\source\repos\ADS\cvicenia\cvicenia\cvicenie02data\cvicenie2data.txt"))
            {
                int row = 0;
                while(!sr.EndOfStream)
                {
                    cities[row++] = int.Parse(sr.ReadLine());
                }

            }
        }
    }
}
