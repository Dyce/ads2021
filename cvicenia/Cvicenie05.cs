﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace cvicenia
{
    public class Cvicenie05
    {
        public Cvicenie05()
        {
            ReadData();
            for(int i = 0; i < matrix.GetLength(0); i++)
            {
                for(int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write($"{matrix[i, j]} ");
                }
                Console.Write("\n");
            }
            GetResult();
        }

        int[,] matrix;

        private void GetResult()
        {
            int maxSubsets = (int)Math.Pow(2, matrix.GetLength(0));

            int[,] bestPaths = new int[maxSubsets, matrix.GetLength(0)];
            

            for(int i = 0; i < maxSubsets; i++)
            {
                for(int j = 0; j < matrix.GetLength(0); j++)
                {
                    bestPaths[i, j] = int.MaxValue;
                }
            }

            List<int> currentList = new List<int>();
            List<int> newList = new List<int>();
            int newset;

            for(int i = 1; i < matrix.GetLength(0); i++)
            {
                newset = (1 << i);
                bestPaths[newset, i] = matrix[0, i];
                currentList.Add(newset);
            }

            for(int len = 2; len < 10; len++)
            {
                foreach(var set in currentList)
                {
                    for(int end = 1; end < matrix.GetLength(0); end++)
                    {
                        if(bestPaths[set,end] != int.MaxValue)
                        {
                            for(int city = 1; city < matrix.GetLength(0); city++)
                            {
                                if((set & (1 << city)) > 0)
                                    continue;
                                newset = set | (1 << city);
                                if(bestPaths[set, end] + matrix[end, city] < bestPaths[newset, city])
                                {
                                    bestPaths[newset, city] = bestPaths[set, end] + matrix[end, city];
                                }
                                if(!newList.Contains(newset))
                                    newList.Add(newset);
                            }
                        }
                    }
                }
                currentList.Clear();
                currentList.AddRange(newList);
                newList.Clear();
            }

            int min = int.MaxValue;
            foreach(var set in currentList)
            {
                for(int end = 1; end < matrix.GetLength(0); end++)
                {
                    if(bestPaths[set, end] != int.MaxValue)
                    {
                        if(bestPaths[set, end] + matrix[end, 0] < min)
                        {
                            min = bestPaths[set, end] + matrix[end, 0];
                        }
                        
                    }
                }
            }

            Console.WriteLine($"Shortest path cost: {min}");
        }
        private void ReadData()
        {
            using(StreamReader sr = new StreamReader(@"C:\Users\Michal\source\repos\ADS\cvicenia\cvicenia\cvicenie05data\cvicenie5data.txt"))
            {
                string stringData = sr.ReadLine();
                int n = int.Parse(stringData);
                matrix = new int[n, n];
                int row = 0;
                while(!sr.EndOfStream)
                {
                    string[] rowData= sr.ReadLine().Split(' ');
                    for(int i = 0; i < rowData.Length; i++)
                    {
                        matrix[row, i] = int.Parse(rowData[i]);
                    }
                    row++;
                }
            }
        }
    }
}
