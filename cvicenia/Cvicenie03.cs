﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace cvicenia
{
    public class Cvicenie03
    {
        public Cvicenie03()
        {
            ReadData();
            Console.WriteLine(GetResult());
        }

        private int GetResult()
        {
            for(int i = 1; i < 1000; i++)
            {
                for(int k = 0; k < 50; k++)
                {
                    if(k == 0)
                        matrix[i, k] += Math.Min(matrix[i - 1, k], matrix[i - 1, k + 1]);
                    else if(k == 49)
                        matrix[i, k] += Math.Min(matrix[i - 1, k], matrix[i - 1, k - 1]);
                    else
                        matrix[i,k] += Math.Min(matrix[i - 1, k], Math.Min(matrix[i - 1, k - 1], matrix[i - 1, k + 1]));
                }
            }
            int minimum = int.MaxValue;
            for(int j = 0; j < 50; j++)
                minimum = minimum < matrix[999, j] ? minimum : matrix[999, j];
            return minimum;
        }

        int[,] matrix = new int[1000, 50];

        private void ReadData()
        {
            using(StreamReader sr = new StreamReader(@"C:\Users\Michal\source\repos\ADS\cvicenia\cvicenia\cvicenie03data\ADS2021_cvicenie3data.txt"))
            {
                int row = 0;
                while(!sr.EndOfStream)
                {
                    string[] rowData = sr.ReadLine().Split(' ');
                    int column = 0;
                    foreach(string s in rowData)
                    {
                        matrix[row, column++] = int.Parse(s);
                    }
                    row++;
                }
            }
        }
    }
}
