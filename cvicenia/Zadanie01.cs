﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using System.Diagnostics.CodeAnalysis;

namespace cvicenia
{
    public class Zadanie01
    {
        public Zadanie01()
        {
            ReadData();
            
            CalculateProbability();
            Console.WriteLine($"Sum of probabilities: {SumProbabilities()}");
            
            BuildTree();
            //keys.RemoveAt(0);
            int comparisons;
            int sumComparisons = 0;
            float sumPrice = 0;
            int[] histogram = new int[10];
            List<List<string>> levels = new List<List<string>>();
            for(int i = 0; i < 10; i++)
            {
                levels.Add(new List<string>());
            }
            
            Console.WriteLine($"Number of key words: {keys.Count}");
            Console.WriteLine($"{"Word",-15}\t{"Comparisons",-4}");
            
            Console.WriteLine("----------------------------");
            foreach(Key key in keys.Where(x => x.Word != ""))
            {
                comparisons = FindWord(key.Word, 1, root.GetLength(0) - 1, 0);
                levels[comparisons-1].Add(key.Word);
                float price = comparisons * (key.P + key.Q);
                Console.WriteLine($"{key.Word,-13}\t{"| "+comparisons,-4}");
                sumComparisons += comparisons;
                sumPrice += price;
                histogram[comparisons-1]++;
            }
            Console.WriteLine("----------------------------");
            
            Console.WriteLine($"Total comparisons: {sumComparisons}");
            Console.WriteLine($"Total price: {sumPrice}\n");
            
            for(int i = 0; i < histogram.Length; i++)
            {
                Console.WriteLine($"Comparisons: {i+1,-2} Count: {histogram[i],-2}");
            }

            for(int i = 0; i < levels.Count; i++)
            {
                Console.WriteLine($"Level {i+1}");
                Console.WriteLine(string.Join(',', levels[i].ToArray()));
            }
            string input = "";
            Console.Write("\n\nZadajte slovo, ktoré sa má vyhľadať: ");
            while((input = Console.ReadLine()) != null)
            {
                int pocetPorovnani = pocet_porovnani(input);
                if(pocetPorovnani == -1)
                    Console.WriteLine("Toto slovo sa v slovníku nenachádza.");
                else
                    Console.WriteLine($"Počet porovnaní: {pocetPorovnani}");
                Console.Write("\nZadajte slovo, ktoré sa má vyhľadať: ");
            }
        }

        private sealed class Key
        {
            public string Word { get; set; }
            public int Index { get; set; }
            public float Q { get; set; }
            public float P { get; set; }
        }

        SortedDictionary<string, int> data = new SortedDictionary<string, int>(new CustomComparer());
        //SortedDictionary<string, int> data = new SortedDictionary<string, int>();
        List<Key> keys = new List<Key>();
        int[,] root;
        int WordCount = 0;

        private int pocet_porovnani(string word)
        {
            if(!string.IsNullOrWhiteSpace(word) && keys.Any(w => w.Word == word))
                return FindWord(word, 1, root.GetLength(0) - 1, 0);
            else
                return -1;
        }


        private int FindWord(string word, int start, int end, int pocet)
        {
            Key key = keys[root[start, end]];

            switch(string.Compare(word, key.Word, StringComparison.OrdinalIgnoreCase))
            {
                case int value when value == 0:
                    return ++pocet;
                case int value when value < 0:
                    return FindWord(word, start, keys.IndexOf(key) - 1, ++pocet);
                case int value when value > 0:
                    return FindWord(word, keys.IndexOf(key) + 1, end, ++pocet);
                default:
                    Console.WriteLine("??!?!!");
                    return -1;
            }
        }

        //algoritmus prebratý od Cormena
        private void BuildTree()
        {
            int n = keys.Count - 1;
            float[,] e = new float[n + 2, n + 1];
            float[,] w = new float[n + 2, n + 1];

            for(int i = 1; i < n + 2; i++)
            {
                e[i, i - 1] = keys[i - 1].Q;
                w[i, i - 1] = keys[i - 1].Q;
            }

            root = new int[n + 1, n + 1];
            for(int l = 1; l < n + 1; l++)
            {
                for(int i = 1; i < n + 2 - l; i++)
                {
                    int j = i + l - 1;
                    e[i, j] = int.MaxValue;
                    w[i, j] = w[i, j - 1] + keys[j].P + keys[j].Q;
                    for(int r = i; r < j + 1; r++)
                    {
                        float t = e[i, r - 1] + e[r + 1, j] + w[i, j];
                        if(t < e[i, j])
                        {
                            e[i, j] = t;
                            root[i, j] = r;
                        }
                    }
                }
            }
        }

        private float SumProbabilities()
        {
            float sum = 0.0f;
            foreach(var item in keys)
            {
                sum += item.P;
                sum += item.Q;
            }
            return sum;
        }

        int totalDummy = 0;
        int totalKey = 0;

        private void CalculateProbability()
        {
            int sum = 0;
            int index = 0;
            float p = 0.0f;
            List<string> medzi = new List<string>();
            string lastWord = "";
            foreach(KeyValuePair<string, int> word in data)
            {
                if(word.Value <= 50000)
                {
                    sum += word.Value;
                    medzi.Add(word.Key);
                    totalDummy += word.Value;
                }
                else
                {
                    keys.Add(new Key
                    {
                        Index = index,
                        P = p,
                        Q = (float)sum / WordCount,
                        Word = lastWord
                    });
                    sum = 0;
                    totalKey += word.Value;
                    lastWord = word.Key;
                    p = (float)word.Value / WordCount;
                    string wat = string.Join(',', medzi.ToArray());
                    medzi.Clear();
                }
                index++;
            }
            keys.Add(new Key
            {
                Index = index++,
                P = p,
                Q = (float)sum / WordCount,
                Word = lastWord
            });
            Console.WriteLine($"Sum of key words ({totalKey}) and dummy words ({totalDummy}) is {totalKey+totalDummy}");
        }


        
        private void ReadData()
        {
            using(StreamReader sr = new StreamReader(@"C:\Users\Michal\source\repos\ADS\cvicenia\cvicenia\zadanie01data\dictionary.txt"))
            {
                while(!sr.EndOfStream)
                {
                    string[] rowData = sr.ReadLine().Split(' ');
                    int count = int.Parse(rowData[0]);
                    data.Add(rowData[1], count);
                    WordCount += count;
                }
            }
        }
    }


    public class CustomComparer : IComparer<string>
    {
        public int Compare([AllowNull] string x, [AllowNull] string y)
        {
            return string.Compare(x, y, StringComparison.OrdinalIgnoreCase);
        }
    }
}
